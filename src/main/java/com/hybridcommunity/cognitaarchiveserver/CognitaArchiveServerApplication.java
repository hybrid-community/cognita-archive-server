package com.hybridcommunity.cognitaarchiveserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CognitaArchiveServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CognitaArchiveServerApplication.class, args);
    }

}
